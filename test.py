from web3 import Web3
from flask import Flask, request, jsonify


# Initialize endpoint URL
node_url = "https://ethereum-sepolia.core.chainstack.com/f3fb7fe97698b20107896136571176d4"
app = Flask(__name__)
# Create the node connection
web3 = Web3(Web3.HTTPProvider(node_url))
gas_limit = 500000
# Verify if the connection is successful
if web3.is_connected():
    print("-" * 50)
    print("Connection Successful")
    print("-" * 50)
else:
    print("Connection Failed")

abi = '[	{		"inputs": [			{				"internalType": "address",				"name": "_sender",				"type": "address"			}		],		"name": "acceptOffer",		"outputs": [],		"stateMutability": "nonpayable",		"type": "function"	},	{		"inputs": [],		"stateMutability": "nonpayable",		"type": "constructor"	},	{		"anonymous": false,		"inputs": [			{				"indexed": true,				"internalType": "address",				"name": "sender",				"type": "address"			},			{				"indexed": true,				"internalType": "address",				"name": "receiver",				"type": "address"			}		],		"name": "OfferAccepted",		"type": "event"	},	{		"anonymous": false,		"inputs": [			{				"indexed": true,				"internalType": "address",				"name": "sender",				"type": "address"			},			{				"indexed": true,				"internalType": "address",				"name": "receiver",				"type": "address"			}		],		"name": "OfferRejected",		"type": "event"	},	{		"anonymous": false,		"inputs": [			{				"indexed": true,				"internalType": "address",				"name": "sender",				"type": "address"			},			{				"indexed": true,				"internalType": "address",				"name": "receiver",				"type": "address"			},			{				"indexed": false,				"internalType": "string",				"name": "message",				"type": "string"			}		],		"name": "OfferSent",		"type": "event"	},	{		"inputs": [			{				"internalType": "string",				"name": "_linkedinAccount",				"type": "string"			}		],		"name": "registerAsTalentAcquisition",		"outputs": [],		"stateMutability": "nonpayable",		"type": "function"	},	{		"inputs": [			{				"internalType": "string",				"name": "_linkedinAccount",				"type": "string"			}		],		"name": "registerAsUser",		"outputs": [],		"stateMutability": "nonpayable",		"type": "function"	},	{		"inputs": [			{				"internalType": "address",				"name": "_sender",				"type": "address"			}		],		"name": "rejectOffer",		"outputs": [],		"stateMutability": "nonpayable",		"type": "function"	},	{		"inputs": [			{				"internalType": "address",				"name": "_receiver",				"type": "address"			},			{				"internalType": "string",				"name": "_message",				"type": "string"			}		],		"name": "sendOffer",		"outputs": [],		"stateMutability": "nonpayable",		"type": "function"	},	{		"inputs": [			{				"internalType": "address",				"name": "_newOwner",				"type": "address"			}		],		"name": "transferOwnership",		"outputs": [],		"stateMutability": "nonpayable",		"type": "function"	},	{		"inputs": [],		"name": "getTopTalentAcquisitions",		"outputs": [			{				"internalType": "address[10]",				"name": "",				"type": "address[10]"			}		],		"stateMutability": "view",		"type": "function"	},	{		"inputs": [			{				"internalType": "address",				"name": "",				"type": "address"			}		],		"name": "talentAcquisitions",		"outputs": [			{				"internalType": "string",				"name": "linkedinAccount",				"type": "string"			},			{				"internalType": "bool",				"name": "exists",				"type": "bool"			},			{				"internalType": "uint256",				"name": "acceptedOffersCount",				"type": "uint256"			}		],		"stateMutability": "view",		"type": "function"	},	{		"inputs": [			{				"internalType": "uint256",				"name": "",				"type": "uint256"			}		],		"name": "topTalentAcquisitions",		"outputs": [			{				"internalType": "address",				"name": "",				"type": "address"			}		],		"stateMutability": "view",		"type": "function"	},	{		"inputs": [			{				"internalType": "address",				"name": "",				"type": "address"			}		],		"name": "users",		"outputs": [			{				"internalType": "string",				"name": "linkedinAccount",				"type": "string"			},			{				"internalType": "bool",				"name": "exists",				"type": "bool"			}		],		"stateMutability": "view",		"type": "function"	}]'

contract_address = "0x458c2778a0Ccf7CBcc9Dd9923e9be36791557740"

contract = web3.eth.contract(address=contract_address, abi=abi)

Chain_id = web3.eth.chain_id


def register_as_ta(private_key_ta,talent_acquisition_address):
    nonce_ta = web3.eth.get_transaction_count(talent_acquisition_address)
    call_function = contract.functions.registerAsTalentAcquisition("TA link").build_transaction({"chainId": Chain_id, "from": talent_acquisition_address, "nonce": nonce_ta})
    signed_tx = web3.eth.account.sign_transaction(call_function, private_key=private_key_ta)
    send_tx = web3.eth.send_raw_transaction(signed_tx.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(send_tx)
    print(tx_receipt) # Optional


def register_as_user(private_key_user,user_address):
    nonce_user = web3.eth.get_transaction_count(user_address)
    call_function = contract.functions.registerAsUser("User link").build_transaction({"chainId": Chain_id, "from": user_address, "nonce": nonce_user})
    signed_tx = web3.eth.account.sign_transaction(call_function, private_key=private_key_user)
    send_tx = web3.eth.send_raw_transaction(signed_tx.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(send_tx)
    print(tx_receipt) # Optional

def send_offer(user_address, talent_acquisition_address, private_key_ta, offer_message):
    nonce_ta = web3.eth.get_transaction_count(talent_acquisition_address)    
    call_function = contract.functions.sendOffer(user_address, offer_message).build_transaction({"chainId": Chain_id, "from": talent_acquisition_address, "nonce": nonce_ta})
    signed_tx = web3.eth.account.sign_transaction(call_function, private_key=private_key_ta)
    send_tx = web3.eth.send_raw_transaction(signed_tx.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(send_tx)
    print(tx_receipt)

def accept_offer(user_address, talent_acquisition_address, private_key_user):
    nonce_user = web3.eth.get_transaction_count(user_address)    
    call_function = contract.functions.acceptOffer(talent_acquisition_address).build_transaction({"chainId": Chain_id, "from": user_address, "nonce": nonce_user})
    signed_tx = web3.eth.account.sign_transaction(call_function, private_key=private_key_user)
    send_tx = web3.eth.send_raw_transaction(signed_tx.rawTransaction)
    tx_receipt = web3.eth.wait_for_transaction_receipt(send_tx)
    print(tx_receipt)

# register_as_ta(private_key_ta=PrivateKeyTa,talent_acquisition_address=TalentAcquisitionAddress)
# register_as_user(private_key_user=PrivateKeyUser,user_address=UserAddress)
# send_offer(user_address=UserAddress,talent_acquisition_address=TalentAcquisitionAddress,private_key_ta=PrivateKeyTa,offer_message="offer message")
# accept_offer(user_address=UserAddress, talent_acquisition_address=TalentAcquisitionAddress,private_key_user=PrivateKeyUser)


@app.route('/register_as_ta', methods=['POST'])
def register_ta():
    TalentAcquisitionAddress = request.json.get('ta_address')
    PrivateKeyTa = request.json.get('pk_ta')
    tx_receipt = register_as_ta(PrivateKeyTa, TalentAcquisitionAddress)
    return jsonify(tx_receipt=tx_receipt)

@app.route('/register_as_user', methods=['POST'])
def register_user():
    UserAddress = request.json.get('user_address')
    PrivateKeyUser = request.json.get('pk_user')
    tx_receipt = register_as_user(PrivateKeyUser, UserAddress)
    return jsonify(tx_receipt=tx_receipt)

@app.route('/send_offer', methods=['POST'])
def send_offer_endpoint():
    user_address = request.json.get('user_address')
    TalentAcquisitionAddress = request.json.get('ta_address')
    PrivateKeyTa = request.json.get('pk_ta')
    tx_receipt = send_offer(user_address, TalentAcquisitionAddress, PrivateKeyTa, "offer-message")
    return jsonify(tx_receipt=tx_receipt)

@app.route('/accept_offer', methods=['POST'])
def accept_offer_endpoint():
    user_address = request.json.get('user_address')
    TalentAcquisitionAddress = request.json.get('ta_address')
    PrivateKeyUser = request.json.get('pk_user')
    tx_receipt = accept_offer(user_address, TalentAcquisitionAddress, PrivateKeyUser)
    return jsonify(tx_receipt=tx_receipt)

if __name__ == '__main__':
    app.run(debug=True)
